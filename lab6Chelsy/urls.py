"""lab6Chelsy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from lab_6.views import index
from profile_page.views import profile as profile_page
from story_9.views import *
from story_10.views import *
from story_11.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name="landing_page"),
    path('profile_page/', profile_page, name="profile_page"),
    path('story9/', main, name="story9"),
    path('add/', add_favorite, name="add-favorites"),
    path('remove/', remove_favorite, name="remove-favorites"),
    path('story10/', index10, name="story10"),
    path('postForm/', postForm, name="postForm"),
    path('emailCheck/', emailCheck, name="emailCheck"),
    path('dataSubscriber/', dataSubscriber, name="dataSubs"),
    path('resultDataSubscriber/', resultDataSubscriber, name="resultDataSubs"),
    path('deleteSubscriber/', deleteSubscriber, name="deleteSubs"),
    path('login/', login, name="loginPage"),
    path('logout/', logout, name="logoutPage"),
    path('data/', get_data, name="get_data"),
]
