import requests
from django.shortcuts import render
from .models import BukuFavorit
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse
from django.conf import settings

response = {}
def main(request):
    if 'user_id' not in request.session.keys():
        return HttpResponseRedirect(reverse('loginPage'))

    if 'books' in request.session.keys():
        list_book = request.session['books']
    else:
        list_book = []

    response['name'] = request.session['name']
    response['count'] = len(list_book)
    return render(request, 'main.html', response)

def get_data(request):
    if 'user_id' not in request.session.keys():
        return HttpResponseRedirect(reverse('login-page'))
    book_api_url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
    book_json = requests.get(book_api_url).json()
    book_items = book_json['items']
    book_list = []

    for book in book_items:
        book_id = book['id']
        if book_id in request.session['books']:
            is_favorited = True
        else:
            is_favorited = False
        book_dict = {"url": book['volumeInfo']['imageLinks']['thumbnail'],
                     "title": book['volumeInfo']['title'], "authors": book['volumeInfo']['authors'][0],
                     "published": book['volumeInfo']['publishedDate'],
                     "id": book['id'], 'boolean': is_favorited}
        book_list.append(book_dict)
    return JsonResponse({'data': book_list})


def add_favorite(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('loginPage'))

    if request.method == 'POST':
        book_id = request.POST['id']
        if 'books' not in request.session.keys():
            request.session['books'] = [book_id]
            size = 1
        else:
            books = request.session['books']
            books.append(book_id)
            request.session['books'] = books
            size = len(books)
        return JsonResponse({'count': size, 'id': book_id})


def remove_favorite(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('loginPage'))

    if request.method == 'POST':
        book_id = request.POST['id']
        books = request.session['books']
        books.remove(book_id)
        request.session['books'] = books
        size = len(books)
        return JsonResponse({'count': size, 'id': book_id})
