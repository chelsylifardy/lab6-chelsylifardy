from django.test import TestCase, Client
from django.urls import resolve
from django.http import request
from .views import main
from .models import BukuFavorit


# Create your tests here.
class UnitTestStory9(TestCase):
    def test_url_main_page_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 302)

    def test_main_page_using_index_function(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, main)

    def test_model_can_add_favorite(self):
        BukuFavorit.objects.create(id_buku='apapun')

        counting_all_status_message = BukuFavorit.objects.all().count()
        self.assertEqual(counting_all_status_message, 1)

    def test_add_and_remove_to_and_from_favorite(self):
        response = Client().post('/add/', data={"id_buku": "apapun"})
        self.assertEqual(response.status_code, 302)
        response = Client().post('/remove/', data={"id_buku": "apapun"})
        self.assertEqual(response.status_code, 302)
