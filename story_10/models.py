from django.db import models

class SubscribeModel(models.Model):
    nama = models.CharField(max_length=200)
    email = models.CharField(max_length=200, unique=True)
    password = models.CharField(max_length=50)