from django import forms

class SubscribeForm(forms.Form):
    nama = forms.CharField(label = "Name", max_length=200, widget=forms.TextInput(attrs = {'class':'form-control', 'id':'nama', 'placeholder':'Full name'}))
    email = forms.EmailField(label = "Email", max_length=200, widget=forms.EmailInput(attrs = {'class':'form-control', 'id':'email', 'placeholder':'Eg. example@email.com'}))
    password = forms.CharField(label = "Password", max_length=50, widget=forms.PasswordInput(attrs = {'class':'form-control', 'id':'password', 'placeholder':'Password'}))