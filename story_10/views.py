from django.shortcuts import render
from .models import SubscribeModel
from .forms import SubscribeForm
from django.http import JsonResponse

def index10(request):
    subscribeData = SubscribeForm()
    response = {'subscribe': subscribeData}
    return render(request, "index10.html", response)

def emailCheck(request):
    if request.method == "POST":
        email = request.POST['email']
        if SubscribeModel.objects.filter(email=email).exists():
            return JsonResponse({'emailUnique':False})
        return JsonResponse({'emailUnique':True})

def postForm(request):
    if request.method == "POST":
        nama = request.POST['nama']
        email = request.POST['email']
        password = request.POST['password']
        SubscribeModel.objects.create(nama=nama, email=email, password=password)
        return JsonResponse({'dataSaved':True})

def dataSubscriber(request):
    if request.method == 'POST':
        dataSubscribe = SubscribeModel.objects.all().values()
        listDataSubscribe = list(dataSubscribe)
        return JsonResponse({'listData': listDataSubscribe})

def resultDataSubscriber(request):
    return render(request, 'subscriber.html')

def deleteSubscriber(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        data = SubscribeModel.objects.filter(email=email, password=password)
        if data.exists():
            data.delete()
            return JsonResponse({'deleted': True})
        return JsonResponse({'deleted': False})
