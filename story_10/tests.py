from django.db import IntegrityError
from django.test import TestCase, Client
from django.urls import resolve
from .views import index10, emailCheck, postForm
from .models import SubscribeModel


# Creating test collaborate with George Matthew Limongan
class Story10UnitTest(TestCase):
    def test_url_subscribe_page_is_exist(self):
        response = Client().get('/story10/')
        self.assertEqual(response.status_code, 200)

    def test_url_subscribe_using_function(self):
        found_story10 = resolve('/story10/')
        found_emailCheck = resolve('/emailCheck/')
        found_postForm = resolve('/postForm/')
        self.assertEqual(found_story10.func, index10)
        self.assertEqual(found_emailCheck.func, emailCheck)
        self.assertEqual(found_postForm.func, postForm)

    def test_model_can_save_data(self):
        SubscribeModel.objects.create(nama="test", email='example@mail.com', password="qwerty")

        counting_all_data = SubscribeModel.objects.all().count()
        self.assertEqual(counting_all_data, 1)

    def test_unique_email(self):
        SubscribeModel.objects.create(email="example@email.com")
        with self.assertRaises(IntegrityError):
            SubscribeModel.objects.create(email="example@email.com")


    def test_check_email_already_exist_view_get_return_200(self):
        SubscribeModel.objects.create(nama="chelsy",
                                        email="chelsy@gmail.com",
                                        password="lalalala")
        response = Client().post('/emailCheck/', data={
            "email": "chelsy@gmail.com"
        })
        self.assertEqual(response.json()['emailUnique'], False)

    def test_post_using_ajax(self):
        response = Client().post('/postForm/', data={
            "nama": "chelsy",
            "email": "chelsy@gmail.com",
            "password": "lalalala",
        })
        self.assertEqual(response.status_code, 200)

    def test_check_email_view_get_return_200(self):
        response = Client().post('/emailCheck/', data={
            "email": "chelsy@gmail.com"
        })
        self.assertEqual(response.status_code, 200)
