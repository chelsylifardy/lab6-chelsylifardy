$(document).ready(function(){
    $(window).on('load', function () {
        $('#status').delay(800).fadeOut();
        $('#preloader').delay(1000).fadeOut('slow');
    });

    $("#accordion").accordion({
        collapsible: true,
        active: false
    });
    
    document.getElementById('grayButton').onclick = switchGray;
    document.getElementById('whiteButton').onclick = switchWhite;
    document.getElementById('blueButton').onclick = switchBlue;
    document.getElementById('yellowButton').onclick = switchYellow;

    function switchGray() {
        document.getElementsByTagName('body')[0].style.backgroundColor = 'gray'; 
        document.getElementsByTagName('body')[0].style.color = 'white';
        document.getElementsByTagName('body')[0].style.color = 'white';
        document.getElementsByTagName('h3')[0].style.color = 'white';
        document.getElementsByTagName('h1')[0].style.color = 'white';
        document.getElementsByTagName('button')[0].style.backgroundColor = 'darksalmon';
        document.getElementsByTagName('button')[1].style.backgroundColor =  'darksalmon';
        $('.form-control').css('border-color', 'darksalmon');
        $('.res-title p').css('color', 'white');
        $('.res-status').css('color', 'black');
        $('.res-status').css('background-color', 'darksalmon');   
    }

    function switchWhite() {
        document.getElementsByTagName('body')[0].style.backgroundColor = 'white';
        document.getElementsByTagName('body')[0].style.color = 'darksalmon';
        document.getElementsByTagName('h3')[0].style.color = 'darksalmon';
        document.getElementsByTagName('h1')[0].style.color = 'darksalmon';
        document.getElementsByTagName('button')[0].style.backgroundColor = 'darksalmon';
        document.getElementsByTagName('button')[1].style.backgroundColor =  'darksalmon';
        $('.form-control').css('border-color', 'darksalmon');
        $('.res-title p').css('color', 'white');
        $('.res-status').css('color', 'black');
        $('.res-status').css('background-color', 'darksalmon');   
    }

    function switchBlue() {
        document.getElementsByTagName('body')[0].style.backgroundColor = 'lightblue';
        document.getElementsByTagName('body')[0].style.color = 'black';
        document.getElementsByTagName('h3')[0].style.color = 'black';
        document.getElementsByTagName('h1')[0].style.color = 'black';
        document.getElementsByTagName('button')[0].style.backgroundColor = 'black';
        document.getElementsByTagName('button')[1].style.backgroundColor =  'black';
        $('.form-control').css('border-color', 'black');
        $('.res-title p').css('color', 'black');
        $('.res-status').css('color', 'white');
        $('.res-status').css('background-color', 'black'); 
    }

    function switchYellow() {
    document.getElementsByTagName('body')[0].style.backgroundColor = '#ffd41c'; 
    document.getElementsByTagName('body')[0].style.color = 'black';
    document.getElementsByTagName('h3')[0].style.color = 'black';
    document.getElementsByTagName('h1')[0].style.color = 'black';
    document.getElementsByTagName('button')[0].style.backgroundColor = 'black';
    document.getElementsByTagName('button')[1].style.backgroundColor =  'black';
    $('.form-control').css('border-color', 'black');
    $('.res-title p').css('color', 'black');
    $('.res-status').css('color', 'white');
    $('.res-status').css('background-color', 'black');
    }
});