var email = "";

function renderResult() {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: 'POST',
        url: '../dataSubscriber/',
        headers: {
            "X-CSRFToken": csrftoken
        },
        success: function (result) {
            var data_length = result.listData.length;
            var data = result.listData;
            for (var i = 0; i < data_length; i++) {
                var name = data[i]['nama'];
                var email = data[i]['email'];
                var button = `<button type="button" class="btn btn-subscribe" data-toggle="modal" data-target="#myModal" onclick="changeEmail(this)" id="${email}">Unsubscribe</button>`;
                var html = `<tr name="${email}"><td>${name}</td><td>${email}</td><td align='center'>${button}</td></tr>`;
                $('#result').append(html);
            }
        }
    })
}

function changeEmail(self) {
    email = self.id;
}

function unsubs() {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var password = $('#input-password').val();
    $.ajax({
        method: 'POST',
        url: '/deleteSubscriber/',
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {email: email, password: password},
        success: function (res) {
            if (res.deleted) {
                $('#close').click();
                $("[name=" + "'" + email + "']").remove();
            } else {
                $('.modal-body .errorlist').replaceWith("<p class='errorlist fail'>Wrong password!</p>");
            }

        }
    })
}

$(function() {
    var timer = null;
    var emailValid;
    
    $('#nama').keydown(function() {
        clearTimeout(timer);
        timer = setTimeout(isFilled, 1000);
        isFilled();
    })

    $('#email').keydown(function() {
        clearTimeout(timer);
        timer = setTimeout(emailAlert, 1000);
        emailAlert();
    })

    $('#password').keydown(function() {
        clearTimeout(timer);
        timer = setTimeout(isFilled, 1000);
        isFilled();
    })

    $('#form').on('submit', function(e) {
        e.preventDefault();
        submitFormData();
    })


    function isFilled() {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        if (($('#password').val().length > 0) && ($('#nama').val().length > 0) && ($('#nama').val().length > 0) && emailValid) {
            $("#btn-subscribe").replaceWith('<button type="submit" id="btn-subscribe" enabled>SUBSCRIBE</button>');
            $("#btn-subscribe-disable").replaceWith('<button type="submit" id="btn-subscribe" enabled>SUBSCRIBE</button>');
            $("#alert-error").replaceWith('<h4 id="alert">Silakan subscribe!</h4>');
            $("#alert").replaceWith('<h4 id="alert">Silakan subscribe!</h4>');
        } else {
            $("#btn-subscribe-disable").replaceWith('<button type="submit" id="btn-subscribe-disable" disabled>SUBSCRIBE</button>');
            $("#btn-subscribe").replaceWith('<button type="submit" id="btn-subscribe-disable" disabled>SUBSCRIBE</button>');
            $("#alert").replaceWith('<h4 id="alert-error">Harap mengisi seluruh form!</h4>');
            $("#alert-error").replaceWith('<h4 id="alert-error">Harap mengisi seluruh form!</h4>');
        }
    }

    function emailAlert() {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: 'POST',
            url: '/emailCheck/',
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {email: $('#email').val()},
            success: function (request) {
                if (request.emailUnique==false && ($('#email').val().length > 0)) {
                    console.log('tst')
                    $("#alert").replaceWith('<h4 id="alert-error">Email Anda sudah pernah digunakan!</h4>');
                    $("#alert-error").replaceWith('<h4 id="alert-error">Email Anda sudah pernah digunakan!</h4>');
                    $("#btn-subscribe").replaceWith('<button type="submit" id="btn-subscribe-disable" disabled>SUBSCRIBE</button>');
                    $("#btn-subscribe-disable").replaceWith('<button type="submit" id="btn-subscribe-disable" disabled>SUBSCRIBE</button>');
                    emailValid = false;
                } else if (request.emailUnique==true && ($('#email').val().length > 0)) {
                    $("#alert-error").replaceWith('<h4 id="alert">Email belum pernah digunakan!</h4>');
                    $("#alert").replaceWith('<h4 id="alert">Email belum pernah digunakan!</h4>');
                    $("#btn-subscribe").replaceWith('<button type="submit" id="btn-subscribe" enabled>SUBSCRIBE</button>');
                    $("#btn-subscribe-disable").replaceWith('<button type="submit" id="btn-subscribe" enabled>SUBSCRIBE</button>');
                    emailValid = true;
                }  else {
                    $("#btn-subscribe").replaceWith('<button type="submit" id="btn-subscribe-disable" disabled>SUBSCRIBE</button>');
                    $("#btn-subscribe-disable").replaceWith('<button type="submit" id="btn-subscribe-disable" disabled>SUBSCRIBE</button>');
                    $("#alert").replaceWith('<h4 id="alert-error">Harap mengisi seluruh form!</h4>');
                    $("#alert-error").replaceWith('<h4 id="alert-error">Harap mengisi seluruh form!</h4>');
                    emailValid = false;
                }
            },
            error: function () {
                alert("Error!")
            }
        })
    }

    function submitFormData() {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: 'POST',
            url: '/postForm/',
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {email: $('#email').val(), nama: $('#nama').val(), password: $('#password').val()},
            success: function (result) {
                $("#alert").replaceWith('<h4 id="alert">Data Anda telah tersimpan!</h4>');
                $("#btn-subscribe").replaceWith('<button type="submit" id="btn-subscribe-disable" disabled>SUBSCRIBE</button>');
                $('#email').val('');            
                $('#nama').val('');            
                $('#password').val('');
            },
            error: function () {
                alert("Error!")
            }
        })
    }
});
