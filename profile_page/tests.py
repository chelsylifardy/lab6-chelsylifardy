from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import profile

# Create your tests here.
class ProfilePage(TestCase):
    def test_profile_page_url_is_exist(self):
        response = Client().get(reverse('profile_page'))
        self.assertEqual(response.status_code, 200)

    def test_profile_page_using_index_func(self): #cek fungsi di views.py ada atau ngggak
        found = resolve(reverse('profile_page'))
        self.assertEqual(found.func, profile)

    def test_profile_page_has_my_name(self):
        response = Client().get(reverse('profile_page'))
        html_response=response.content.decode('utf8')
        self.assertIn('Chelsy', html_response)

    def test_profile_page_has_my_npm(self):
        response = Client().get(reverse('profile_page'))
        html_response=response.content.decode('utf8')
        self.assertIn('1706043582', html_response)