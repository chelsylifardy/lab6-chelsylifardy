from django.shortcuts import render
from .forms import Post_Status
from .models import Post
from django.http import HttpResponseRedirect


def index(request):
    if  request.method == 'POST':
        post_status = Post_Status(request.POST)
        if post_status.is_valid():
            status = post_status.cleaned_data
            Post.objects.create(**status)
            return HttpResponseRedirect('/') #ini redirect ke urls
    else:
        post_status = Post_Status()
    result = list(Post.objects.all())
    result.reverse()
    content = {'res' : result, 'status': post_status}
    return render(request, 'index.html', content)
