from django import forms

class Post_Status(forms.Form):
    status = forms.CharField(label="", max_length=300, widget=forms.Textarea(attrs={'class':'form-control'}))