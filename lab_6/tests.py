import time
from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import index
from .models import Post
from .forms import Post_Status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class Lab6UnitTest(TestCase):

    def test_lab_6_url_is_exist(self):
        response = Client().get(reverse('landing_page'))
        self.assertEqual(response.status_code, 200)
    
    def test_lab_6_using_index_func(self): #cek fungsi di views.py ada atau ngggak
        found = resolve(reverse('landing_page'))
        self.assertEqual(found.func, index)
    
    def test_model_can_create_new_post(self):
        new_post = Post.objects.create(status='status')
        counting_all_available_new_post = Post.objects.all().count()
        self.assertEqual(counting_all_available_new_post,1)

    def test_form_validation_for_blank_items(self):
        form = Post_Status(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ['This field is required.']
        )

    def test_lab_6_post_success_and_render_the_result(self):
        test='Anonymous'
        response_post = Client().post(reverse('landing_page'), {'status':test})
        self.assertEqual(response_post.status_code, 302)

        response=Client().get(reverse('landing_page'))
        html_response=response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_lab_6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post(reverse('landing_page'), {'status':''})
        self.assertNotEqual(response_post.status_code, 302)

        response=Client().get(reverse('landing_page'))
        html_response=response.content.decode('utf8')
        self.assertNotIn(test, html_response)


class Lab6FunctionalTest(TestCase):
    def setUp (self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab6FunctionalTest,self).setUp()

    def test_input_todo(self):
        selenium = self.selenium
        selenium.get('http://chitchat-status.herokuapp.com/')
        status = selenium.find_element_by_id('id_status')
        post_btn = selenium.find_element_by_id('btn-post')

        status.send_keys('Coba Coba')
        post_btn.send_keys(Keys.RETURN)

        self.assertIn("Coba Coba", selenium.page_source)

    def test_layout_title_in_head(self):
        selenium = self.selenium
        selenium.get('http://chitchat-status.herokuapp.com/')
        title = selenium.find_element_by_id("title").text
        head = selenium.find_element_by_id("head").text

        self.assertIn(title,head)
    
    def test_layout_intro_in_body(self):
        selenium = self.selenium
        selenium.get('http://chitchat-status.herokuapp.com/')
        intro = selenium.find_element_by_id("intro").text
        body = selenium.find_element_by_id("body").text

        self.assertIn(intro,body)

    def test_style_in_element_font(self):
        selenium = self.selenium
        selenium.get('http://chitchat-status.herokuapp.com/')
        font_color = selenium.find_element_by_id("intro").value_of_css_property("color")

        self.assertEqual(font_color, "rgba(233, 150, 122, 1)")

    def test_style_in_element_button(self):
        selenium = self.selenium
        selenium.get('http://chitchat-status.herokuapp.com/')
        button = selenium.find_element_by_id("btn-post").value_of_css_property("background-color")

        self.assertEqual(button, "rgba(233, 150, 122, 1)")

    def test_accordion_is_clickable(self):
        selenium = self.selenium
        selenium.get('http://chitchat-status.herokuapp.com/profile_page')
        accordion = selenium.find_element_by_id('ui-id-1')

        accordion.send_keys(Keys.RETURN)
        self.assertIn("Daftar Aktivitas", selenium.page_source)
    
    def test_change_theme(self):
        selenium = self.selenium
        selenium.get("http://chitchat-status.herokuapp.com/")
        button = selenium.find_element_by_id('grayButton')
        button.send_keys(Keys.RETURN)
        heading = selenium.find_element_by_tag_name('h1').value_of_css_property("color")
        self.assertEqual(heading, 'rgba(255, 255, 255, 1)')
        
    def tearDown(self):
        time.sleep(5)
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()


